var app = angular.module(
    'app',
    [
        'ngRoute',
        'ngResource',
        'ngToast',
        'ngSanitize',
        'connexionMod'
    ]
);

var connexionMod = angular.module('connexionMod',[]);

app.config(function($routeProvider, $locationProvider){

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/connexion',{
            templateUrl: '/views/connexion.html',
            controller: 'AuthenticationController'
        })
        .when('/espace-commercial/accueil/',{
            templateUrl: '/views/espace-commercial/accueil_commmercial.html',
            controller: 'AccueilCommercialController'
        })
        .when('/espace-commercial/nouveau-client/',{
            templateUrl: '/views/espace-commercial/nouveau_client.html',
            controller: 'NouveauClientController'
        })
        .when('/espace-commercial/nouveau-devis/client/:idClient',{
            templateUrl: '/views/espace-commercial/devisCreator.html',
            controller: 'NouveauDevisController'
        })
        .when('/espace-be/accueil/',{
            templateUrl: '/views/espace-be/accueil_be.html',
            controller: 'AccueilBEController'
        })
        .when('/espace-be/gestion-module/',{
            templateUrl: '/views/espace-be/gestion_module.html',
            controller: 'GestionModuleController'
        })
        .when('/espace-commercial/choix-nouveau-devis/',{
            templateUrl: '/views/espace-commercial/choix_nouveau_devis.html',
            controller: 'AccueilCommercialController'
        })
        .when('/espace-commercial/nouveau-devis/choix-client',{
            templateUrl: '/views/espace-commercial/choix_client_devis.html',
            controller: 'AccueilCommercialController'
        })
        .when('/espace-commercial/finalisation-devis/:id',{
            templateUrl: '/views/espace-commercial/finalisation_devis.html',
            controller: 'FinalisationDevisController'
        })
        .when('/espace-commercial/devis/:id',{
            templateUrl: '/views/espace-commercial/affichage_devis.html',
            controller: 'AffichageDevisController'
        })
        .otherwise(
            { redirectTo: '/connexion'}
        );
});

//fonctions de récupération de données
function getUser(){
    var user = null;
    $.ajax({
        url: "/auth",
        async : false,
        success: function(response,xhr){
            if(xhr === "success"){
                user = response;
            }
        }
    });
    return user;
}

function getAllDepartements() {
    var departements = [];
    $.ajax({
        url: "/departements",
        async : false,
        success: function(response){
            departements = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des départements : "+error);
        }
    });
    return departements;
}
function getCommunesByIdDepartement(idDep){
    var communes = [];
    $.ajax({
        url: "/communes/dep/"+idDep,
        async : false,
        success: function(response){
            communes = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des villes : "+error);
        }
    });
    return communes;
}

function getAllPays(){
    var pays = [];
    $.ajax({
        url: "/pays",
        async : false,
        success: function(response){
            pays = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des pays : "+error);
        }
    });
    return pays;
}

function getClientById(idClient){
    var client = null;
    $.ajax({
        url: "/clients/"+idClient,
        async : false,
        success: function(response){
            client = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération d'un client : "+error);
        }
    });
    return client;
}

function getClients(){
    var clients = null;
    $.ajax({
        url: "/clients",
        async : false,
        success: function(response){
            clients = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération d'un client : "+error);
        }
    });
    return clients;
}

function getAllCategoriesModules(){
    var categoriesModules = null;
    $.ajax({
        url: "/categoriesModules",
        async : false,
        success: function(response){
            categoriesModules = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des catégories des modules : "+error);
        }
    });
    return categoriesModules;
}

function getAllSousCategoriesModulesByCategorieModule(categorie){
    var sousCategoriesModules = null;
    $.ajax({
        url: "/sousCategoriesModules/categorie/"+categorie,
        async : false,
        success: function(response){
            sousCategoriesModules = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des sous categories de modules : "+error);
        }
    });
    return sousCategoriesModules;
}

function getAllModulesBySousCategoriesModules(sousCategorie){
    var modules = null;
    $.ajax({
        url: "/modules/sousCategorieModule/"+sousCategorie,
        async : false,
        success: function(response){
            modules = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des modules d'une sous categorie : "+error);
        }
    });
    return modules;
}

function getDevisById(idDevis){
    var devis = null;
    $.ajax({
        url: "/devis/"+idDevis,
        async : false,
        success: function(response){
            devis = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération d'un devis : "+error);
        }
    });
    return devis;
}

function getAllEtatDevis(){
    var etatsDevis = null;
    $.ajax({
        url: "/etatsDevis",
        async : false,
        success: function(response){
            etatsDevis = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des états de devis : "+error);
        }
    });
    return etatsDevis;
}

function getAllMoyensPaiement(){
    var moyensPaiement = null;
    $.ajax({
        url: "/moyensPaiement",
        async : false,
        success: function(response){
            moyensPaiement = response;
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des moyens de paiement d'un devis : "+error);
        }
    });
    return moyensPaiement;
}

function redirectProfil(profil){
    var path = "";
    switch (profil){
        case 1:
            path = "/#/espace-commercial/accueil/";
            break;
        case 2:
            path = "/#/espace-be/accueil/";
            break;
        default:
            path = "/#/connexion";
            break;
    }
    document.location.replace(path);
}

function editerDevis(idDevis){
    var fileName = "";
    $.ajax({
        url: " /devis/"+idDevis+"/generate",
        async : false,
        success: function(response){
            fileName = "devis_D85DKJDK.pdf";
        },
        error: function (error) {
            console.log("Une erreur est survenue lors de la récupération des moyens de paiement d'un devis : "+error);
        }
    });
    document.location.replace("/doc/"+fileName);

}


