app.service("utilsService", function(){
    this.getUser = function(){
        var user = null;
        $.ajax({
            url: "/auth",
            async : false,
            success: function(response,xhr){
                if(xhr === "success"){
                    user = response;
                }
            }
        });
        return user;
    };
});