app.controller('AccueilCommercialController', function ($scope,$http) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 1) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;

    $scope.clients = getClients();
});

