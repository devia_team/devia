app.controller('AffichageDevisController', function ($scope,$routeParams,$http) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 1) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;

    var idDevis = $routeParams.id;
    $scope.devis = getDevisById(idDevis);

    if($scope.devis===null){
        document.location.replace("/#/espace-commercial/accueil/");
    }

    $scope.coutTotal = 0;
    for(var i = 0; i < $scope.devis.modules.length; i++){
        $scope.coutTotal+= $scope.devis.modules[i].prixHT;
    }

    $scope.EditerDevis = function(){
        swal("Fonction en cours de développement","L'édition d'un devis n'est pas encore disponible sur Devia.").setDefaults({confirmButtonColor: '#ff4500'});
    }
});

