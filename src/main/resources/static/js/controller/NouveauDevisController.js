app.controller('NouveauDevisController', function ($scope,$http,$routeParams) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 1) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.modules = [];
    var idClient = $routeParams.idClient;
    $scope.client = getClientById(idClient);

    $scope.modulesDevis = [];

    $scope.coutTotalDevis = 0;
    $scope.nbModules = 0;

    if($scope.client===null){
        document.location.replace("/#/espace-commercial/nouveau-devis/choix-client");
    }

    $scope.categoriesModules = getAllCategoriesModules();

    $scope.changeCategorieModule = function(){

        $(".module-selected").removeClass("module-selected");
        $scope.moduleSelected = null;

        if($scope.categorieModuleSelected !== null) {
            $scope.sousCategoriesModules = getAllSousCategoriesModulesByCategorieModule($scope.categorieModuleSelected.id);
            $('select').material_select();
        }
    }

    $scope.changeSousCategorieModule = function(){
        if($scope.sousCategorieModuleSelected !== null) {
            $scope.moduleSelected = null;
            $(".module-selected").removeClass("module-selected");
            $scope.modules = getAllModulesBySousCategoriesModules($scope.sousCategorieModuleSelected.id);
        }
    }

    $scope.selectModule = function(module){
        $scope.moduleSelected = module;
        $(".module-selected").removeClass("module-selected");
        $("#Mod"+module.id).addClass( "module-selected" );
    }

    $scope.addModule = function(){
        if(typeof $scope.moduleSelected !== "undefined" && $scope.moduleSelected !== null){
            if(typeof $scope.modulesDevis[$scope.moduleSelected.id] === "undefined" || $scope.modulesDevis[$scope.moduleSelected.id] === null){
                $scope.modulesDevis[$scope.moduleSelected.id] = $scope.moduleSelected;
                $scope.nbModules++;
                $scope.coutTotalDevis+= $scope.moduleSelected.prixHT;
            }
            else {
                swal("Action impossible","Vous ne pouvez insérer qu'une seul occurence de chaque module !").setDefaults({confirmButtonColor: '#ff4500'});
            }
        }
        else {
            swal("Action impossible","Veuillez sélectionner un module parmis la liste !").setDefaults({confirmButtonColor: '#ff4500'});
        }
    }

    $scope.supprimerModule = function(idModule){
        if(typeof $scope.modulesDevis[idModule] !== "undefined" && $scope.modulesDevis[idModule] !== null){
            $scope.coutTotalDevis-= $scope.modulesDevis[idModule].prixHT;
            $scope.modulesDevis[idModule] = null;
            $scope.modulesDevis.splice(idModule, 1);
            $scope.nbModules--;
        }
    }

    $scope.enregistrerDevis = function(){
        if($scope.nbModules > 0){
            $http
            .post("/devis/client/"+$scope.client.id, $scope.modulesDevis)
            .then(
                function (response) {
                    var devis = response.data;
                    document.location.replace("/#/espace-commercial/finalisation-devis/"+devis.id);
                },
                function (response){
                    swal("Erreur serveur","Impossible de créer le devis, veuillez contacter un administrateur !").setDefaults({confirmButtonColor: '#ff4500'});
                }
            );
        }
        else {
            swal("Action impossible","Veuillez ajouter au minimum un module avant de passer à l'étape de validation !").setDefaults({confirmButtonColor: '#ff4500'});
        }
    }

    $scope.abandon = function(){
        document.location.replace("/#/espace-commercial/accueil/");
    }
});

