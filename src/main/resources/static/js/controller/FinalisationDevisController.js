app.controller('FinalisationDevisController', function ($scope,$routeParams,$http) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 1) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;

    var idDevis = $routeParams.id;
    $scope.devis = getDevisById(idDevis);

    if($scope.devis===null){
        document.location.replace("/#/espace-commercial/accueil/");
    }

    $scope.etatsDevis = getAllEtatDevis();

    $scope.moyensPaiement = getAllMoyensPaiement();

    $scope.finaliserDevis = function(){
        $http
            .put("/devis/"+$scope.devis.id,$scope.devis)
            .then(
                function (response) {
                    var devis = response.data;
                    document.location.replace("/#/espace-commercial/devis/"+devis.id);
                },
                function (response){
                    swal("Erreur serveur","Impossible de créer le devis, veuillez contatcter un administrateur !").setDefaults({confirmButtonColor: '#ff4500'});
                }
            );
    }

    $scope.abandonnerDevis = function () {

    }
});

