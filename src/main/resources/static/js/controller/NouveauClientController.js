app.controller('NouveauClientController', function ($scope,$http) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 1) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;

    $scope.departements = getAllDepartements();
    $scope.pays = getAllPays();

    $scope.ajouterClient = function(){
        var client = $scope.client;
        //règles de saisie
        if (typeof client.civilite == 'undefined' || client.civilite === -1) {
            swal("Champs obligatoires","Veuillez spécifier la civilité du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.nom == 'undefined' || client.nom === "") {
            swal("Champs obligatoires","Veuillez spécifier le nom du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.prenom == 'undefined' || client.prenom === "") {
            swal("Champs obligatoires","Veuillez spécifier le prénom du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.dateNaissance == 'dateNaissance') {
            swal("Champs obligatoires","Veuillez spécifier la date de naissance du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.telephone == 'undefined' || client.telephone === "") {
            swal("Champs obligatoires","Veuillez spécifier le numéro de téléphone du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.adresseMail == 'undefined' || client.adresseMail === "") {
            swal("Champs obligatoires","Veuillez spécifier l'adresse e-mail du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.adresse.numero == 'undefined' || client.adresse.numero === "") {
            swal("Champs obligatoires","Veuillez spécifier le numéro de voie de l'adresse du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.adresse.libelleVoie == 'undefined' || client.adresse.libelleVoie === "") {
            swal("Champs obligatoires","Veuillez spécifier la voie de l'adresse du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.adresse.pays == 'undefined' || client.adresse.pays === "") {
            swal("Champs obligatoires","Veuillez spécifier le pays du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof client.adresse.commune == 'undefined' || client.adresse.commune === "") {
            swal("Champs obligatoires","Veuillez spécifier la commune du client !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        client.adresse.id = null;
        $http
            .post("/clients", client)
            .then(
                function (response) {
                    if(response.status === 202){
                        var utilisateur = response.data;
                        redirectProfil(utilisateur.profilPersonne.id);
                    }
                },
                function (response){
                    if(response.status === 403) {
                        swal("Connexion impossible","Aucun compte trouvé avec les paramètres saisis !").setDefaults({confirmButtonColor: '#ff4500'});
                    }
                    else if(response.status === 401) {
                        swal("Connexion impossible","Mot de passe incorrect !").setDefaults({confirmButtonColor: '#ff4500'});
                    }
                }
            );
    }

    $scope.changeDepartement = function(){
        $scope.communes = getCommunesByIdDepartement($scope.departementSelected.id);
        $('select').material_select();
    }
});

