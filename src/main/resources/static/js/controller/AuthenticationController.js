app.controller('AuthenticationController', function ($scope,$http) {
    //si un utilisateur est connecté, profil contient l'identifiant du profil de celui ci
    $scope.utilisateur = getUser();
    if($scope.utilisateur !== null) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = false;
    $scope.connexion = function () {
        var email = $scope.email;
        var password = $scope.password;
        if (typeof email == 'undefined' || email === "") {
            swal("Champs obligatoires","Veuillez spécifier une adresse mail !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        if (typeof password == 'undefined' || password === "") {
            swal("Champs obligatoires","Veuillez spécifier un mot de passe de connexion !").setDefaults({confirmButtonColor: '#ff4500'});
            return;
        }
        var data = {
            adresseMail: email,
            motDePasse: password
        };
        $http
            .post("/auth", data)
            .then(
                function (response) {
                    if(response.status === 202){
                        var utilisateur = response.data;
                        redirectProfil(utilisateur.profilPersonne.id);
                    }
                },
                function (response){
                    if(response.status === 403) {
                        swal("Connexion impossible","Aucun compte trouvé avec les paramètres saisis !").setDefaults({confirmButtonColor: '#ff4500'});
                    }
                    else if(response.status === 401) {
                        swal("Connexion impossible","Mot de passe incorrect !").setDefaults({confirmButtonColor: '#ff4500'});
                    }
                }
            );
    }
});

