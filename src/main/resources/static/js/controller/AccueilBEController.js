app.controller('AccueilBEController', function ($scope, $http) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 2) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;


});

