app.controller('GestionModuleController', function ($scope, $http, ngToast) {
    $scope.utilisateur = getUser();
    if($scope.utilisateur === null){
        redirectProfil(-1);
    }
    else if($scope.utilisateur.profilPersonne.id !== 2) {
        redirectProfil($scope.utilisateur.profilPersonne.id);
    }
    $scope.$parent.authenticate = true;

    $scope.addMode = false;
    function getAllModules() {
        $http({
            method: 'GET',
            url: '/modules'
        }).then(function (data) {
            $scope.modules = data.data;
        }, function (error) {
            ngToast.danger("Une erreur est survenue lors de la récupération des modules.");
        });
    }

    getAllModules();


});

