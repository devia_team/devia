package com.team.devia.controller;

import com.team.devia.entity.Adresse;
import com.team.devia.entity.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    com.team.devia.repository.PersonneRepository personneRepository;

    @Autowired
    com.team.devia.repository.ProfilPersonneRepository profilPersonneRepository;

    @Autowired
    com.team.devia.repository.AdresseRepository adresseRepository;


    @GetMapping()
    public Iterable<Personne> getAll() {
        return personneRepository.findAllByProfilPersonne_Id(new Long((3)));
    }

    @GetMapping("/{idClient}")
    public Personne getById(@PathVariable Long idClient) {
        Personne client = personneRepository.findOne(idClient);
        if(!(client.getProfilPersonne().getId().equals(new Long(3)))){
            client = null;
        }
        return client;
    }

    @PostMapping()
    @ResponseBody
    public Personne saveClient (@RequestBody Personne personne) {
        //controle à effectuer
        personne.setProfilPersonne(profilPersonneRepository.findOne(new Long((3))));
        Adresse nouvelleAdresse = personne.getAdresse();
        nouvelleAdresse = adresseRepository.save(nouvelleAdresse);
        personne.setAdresse(nouvelleAdresse);
        if(personne != null){
            return personneRepository.save(personne);
        }
        return null;
    }
}
