package com.team.devia.controller;

import com.team.devia.entity.CategorieModule;
import com.team.devia.entity.SousCategorieModule;
import com.team.devia.repository.CategorieModuleRepository;
import com.team.devia.repository.SousCategorieModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sousCategoriesModules")
public class SousCategorieModuleController {
    @Autowired
    SousCategorieModuleRepository sousCategorieModuleRepository;

    @GetMapping()
    public Iterable<SousCategorieModule> getAll() {
        return sousCategorieModuleRepository.findAll();
    }

    @GetMapping("/categorie/{idCategorie}")
    public Iterable<SousCategorieModule> getAllByIdCategorie(@PathVariable Long idCategorie) {
        return sousCategorieModuleRepository.findAllByCategorieModule_Id(idCategorie);
    }
}
