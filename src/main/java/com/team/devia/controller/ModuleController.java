package com.team.devia.controller;

import com.team.devia.entity.Module;
import com.team.devia.repository.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/modules")
public class ModuleController {
    @Autowired
    ModuleRepository moduleRepository;

    @GetMapping("/sousCategorieModule/{idSousCategorie}")
    Iterable<Module> getAllByIdSousCategorie(@PathVariable Long idSousCategorie) {
        return moduleRepository.findAllBySousCategorieModule_Id(idSousCategorie);
    }

    @GetMapping()
    public Iterable<Module> readAllModules() {
        Iterable<Module> modules = moduleRepository.findAll();
        return modules;
    }

    @GetMapping("/{id}")
    public Module readModule(@PathVariable Long id) {
        return moduleRepository.findOne(id);
    }

    @PostMapping()
    public Module createModule(@RequestBody Module module) {
        moduleRepository.save(module);
        return module;
    }

    @DeleteMapping("/{id}")
    public void deleteModule(@PathVariable Long id) {
        moduleRepository.delete(id);
    }
}
