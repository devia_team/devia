package com.team.devia.controller;

import com.team.devia.entity.Devis;
import com.team.devia.entity.Module;
import com.team.devia.repository.DevisRepository;
import com.team.devia.repository.EtatDevisRepository;
import com.team.devia.repository.PersonneRepository;
import com.team.devia.repository.ModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;


@RestController
@RequestMapping("/devis")
public class DevisController {
    @Autowired
    DevisRepository devisRepository;

    @Autowired
    ModuleRepository moduleRepository;

    @Autowired
    PersonneRepository personneRepository;

    @Autowired
    EtatDevisRepository etatDevisRepository;

    @GetMapping()
    public Iterable<Devis> readAllDevis() {
        return devisRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Devis readDevis (@PathVariable Long id) {
        return devisRepository.findOne(id);
    }

    @PostMapping("/client/{idClient}")
    @ResponseBody
    public Devis createDevis (@RequestBody List<Module> modules,@PathVariable Long idClient) {
        Devis newDevis = new Devis();

        newDevis.setEtatDevis(etatDevisRepository.findOne(new Long(1)));
        newDevis.setModules(modules);
        newDevis.setPersonne(personneRepository.findOne(idClient));
        newDevis.setDateEmission(Calendar.getInstance().getTime());

        newDevis = devisRepository.save(newDevis);
        return newDevis;
    }

    @DeleteMapping("/{id}")
    public Devis deleteDevis (@PathVariable Long id) {
        Devis p = devisRepository.findOne(id);

        devisRepository.delete(id);
        return p;
    }

    @PutMapping("/{id}")
    public Devis updateDevis (@PathVariable Long id, @RequestBody Devis devis) {
        Devis d = devisRepository.findOne(id);
        d.setEtatDevis(devis.getEtatDevis());
        d.setCommentaire(devis.getCommentaire());
        d.setMoyenPaiement(devis.getMoyenPaiement());
        d = devisRepository.save(d);
        return d;
    }

    @GetMapping("/{id}/generate")
    @ResponseBody
    public void devisToPDF(@PathVariable Long id) {

        Devis devis = devisRepository.findOne((long) 1);

        PDFController.generate(devis);

    }
}
