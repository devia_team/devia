package com.team.devia.controller;

import com.team.devia.entity.CategorieModule;
import com.team.devia.entity.Personne;
import com.team.devia.repository.CategorieModuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categoriesModules")
public class CategorieModuleController {

    @Autowired
    CategorieModuleRepository categorieModuleRepository;

    @GetMapping()
    public Iterable<CategorieModule> getAll() {
        return categorieModuleRepository.findAll();
    }
}
