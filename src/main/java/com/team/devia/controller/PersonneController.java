package com.team.devia.controller;

import com.team.devia.entity.Personne;
import com.team.devia.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/personnes")
public class PersonneController {

    @Autowired
    PersonneRepository PersonneRepository;

    @GetMapping()
    public Iterable<Personne> readAllPersonnes () {
        return PersonneRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Personne readPersonne (@PathVariable Long id) {
        return PersonneRepository.findOne(id);
    }

    @PostMapping()
    @ResponseBody
    public Personne createPersonne (@RequestBody Personne Personne) {
        if(Personne != null){
            PersonneRepository.save(Personne);
            return Personne;
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public Personne deletePersonne (@PathVariable Long id) {
        Personne p = PersonneRepository.findOne(id);

        PersonneRepository.delete(id);
        return p;
    }

    @PutMapping("/{id}")
    public Personne updatePersonne (@PathVariable Long id, @RequestBody Personne Personne) {
        Personne p = PersonneRepository.findOne(id);
        p.setNom(Personne.getNom());
        p.setMotDePasse(Personne.getMotDePasse());
        PersonneRepository.save(p);
        return p;
    }
}



