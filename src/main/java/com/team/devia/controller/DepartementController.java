package com.team.devia.controller;

import com.team.devia.entity.Departement;
import com.team.devia.repository.DepartementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/departements")
public class DepartementController {

    @Autowired
    DepartementRepository departementRepository;

    @GetMapping()
    public Iterable<Departement> getAll() {
        return departementRepository.findAll();
    }
}