package com.team.devia.controller;


import com.team.devia.entity.MoyenPaiement;
import com.team.devia.repository.MoyenPaiementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/moyensPaiement")
public class MoyenPaiementController {
    @Autowired
    MoyenPaiementRepository moyenPaiementRepository;

    @GetMapping()
    public Iterable<MoyenPaiement> getAll() {
        return moyenPaiementRepository.findAll();
    }
}
