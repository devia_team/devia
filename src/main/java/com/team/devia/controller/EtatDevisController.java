package com.team.devia.controller;

import com.team.devia.entity.EtatDevis;
import com.team.devia.repository.EtatDevisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/etatsDevis")
public class EtatDevisController {
    @Autowired
    EtatDevisRepository etatDevisRepository;

    @GetMapping()
    public Iterable<EtatDevis> getAll() {
        return etatDevisRepository.findAll();
    }
}
