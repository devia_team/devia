package com.team.devia.controller;

import com.team.devia.entity.Commune;
import com.team.devia.entity.Departement;
import com.team.devia.repository.CommuneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/communes")
public class CommuneController {

    @Autowired
    CommuneRepository communeRepository;

    @GetMapping()
    public Iterable<Commune> getAll() {
        return communeRepository.findAll();
    }

    @GetMapping("/dep/{idDep}")
    public Iterable<Commune> getByIdDepartement(@PathVariable Long idDep) {
        return communeRepository.findCommuneByDepartement_Id(idDep);
    }

}
