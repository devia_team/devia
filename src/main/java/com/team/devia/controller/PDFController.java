package com.team.devia.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.team.devia.entity.Devis;
import com.team.devia.entity.Module;

import java.io.FileOutputStream;
import java.util.List;

public class PDFController {

    private static String FILE = "C:\\Users\\A637191\\Desktop\\CESI\\MADERA\\LIVRABLE_3\\devia\\src\\main\\resources\\static\\doc\\devis_D85DKJDK.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);

    private static Font titleMadera = new Font(Font.FontFamily.COURIER, 26,
            Font.BOLD, BaseColor.ORANGE);

    public static void generate(Devis devis) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document, devis);
            addTitlePage(document, devis);
            addContent(document, devis);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // iText allows to add metadata to the PDF which can be viewed in your Adobe
    // Reader
    // under File -> Properties
    private static void addMetaData(Document document, Devis devis) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Lars Vogel");
        document.addCreator("Lars Vogel");
    }

    private static void addTitlePage(Document document, Devis devis)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph("MADERA", titleMadera));
        addEmptyLine(preface, 1);

        preface.add(new Paragraph(
                "Entreprise MADERA \n" +
                        "65 avenue LaFontaine \n" +
                        "51100 Reims",
                smallBold));
        addEmptyLine(preface, 2);

        preface.add(new Paragraph(
                "Client : Mr. " +
                        devis.getPersonne().getNom() +
                        " \n Téléphone : " +
                        devis.getPersonne().getTelephone() +
                        "\n Mail : " +
                        devis.getPersonne().getAdresseMail(),
                smallBold));


        addEmptyLine(preface, 3);
        document.add(preface);
    }

    private static void addContent(Document document, Devis devis) throws DocumentException {

        // add a table
        PdfPTable table = createTable(document, devis.getModules());
        document.add(table);
    }

    private static PdfPTable createTable(Document document, List<Module> modules)
            throws BadElementException {
        PdfPTable table = new PdfPTable(5);
        // t.setBorderColor(BaseColor.GRAY);
        // t.setPadding(4);
        // t.setSpacing(4);
        // t.setBorderWidth(1);

        PdfPCell c1 = new PdfPCell(new Phrase("Référence"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Désignation"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Quantité"));
        //c1.setHorizontalAlignment(Element.ALIGN_CENTER);;
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("PU HT"));
        //c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Total"));
        //c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        table.setHeaderRows(1);

        for (Module module : modules) {
            table.addCell(module.getReference());
            table.addCell(module.getLibelle());
            table.addCell("1");
            table.addCell(String.valueOf(module.getPrixHT()));
            table.addCell(module.getTempsPose());
        }

        return table;

    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

}
