package com.team.devia.controller;


import com.team.devia.entity.Personne;
import com.team.devia.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/auth")
public class AuthentificationController {

    @Autowired
    PersonneRepository personneRepository;

    @PostMapping()
    @ResponseBody
    public Personne authentification (@RequestBody Personne personne, HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession(true);

        Personne utilisateur = personneRepository.findByAdresseMail(personne.getAdresseMail());

        int statusReturned;
        if(utilisateur instanceof Personne){
            if(utilisateur.getMotDePasse().equals(personne.getMotDePasse())){
                statusReturned = HttpServletResponse.SC_ACCEPTED;
                session.setAttribute("user",utilisateur);
            }
            else {
                utilisateur = null;
                statusReturned = HttpServletResponse.SC_UNAUTHORIZED;
            }
        }
        else {
            utilisateur = null;
            statusReturned = HttpServletResponse.SC_FORBIDDEN;
        }
        response.setStatus(statusReturned);
        return utilisateur;
    }

    @GetMapping()
    @ResponseBody
    public Personne GetAuthentification (HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession(true);
        Personne utilisateur = (Personne) session.getAttribute("user");
        int statusReturned = HttpServletResponse.SC_ACCEPTED;
        if(!(utilisateur instanceof Personne)){
            utilisateur = null;
            statusReturned = HttpServletResponse.SC_FORBIDDEN;
        }
        response.setStatus(statusReturned);
        return utilisateur;
    }
}
