package com.team.devia.controller;

import com.team.devia.entity.Departement;
import com.team.devia.entity.Pays;
import com.team.devia.repository.PaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pays")
public class PaysController {

    @Autowired
    PaysRepository paysRepository;

    @GetMapping()
    public Iterable<Pays> getAll() {
        return paysRepository.findAll();
    }
}
