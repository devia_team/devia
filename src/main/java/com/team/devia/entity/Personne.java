package com.team.devia.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="personne")
public class Personne {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "email")
    private String adresseMail;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "date_naissance")
    private Date dateNaissance;

    @Column(name = "mot_de_passe")
    private String motDePasse;

    @Column(name = "civilite")
    private String civilite;

    @JoinColumn(name="id_profil_personne")
    @ManyToOne
    private ProfilPersonne profilPersonne;

    @JoinColumn(name="id_adresse")
    @ManyToOne
    private Adresse adresse;

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresseMail() {
        return adresseMail;
    }

    public void setAdresseMail(String adresseMail) {
        this.adresseMail = adresseMail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    public ProfilPersonne getProfilPersonne() {
        return profilPersonne;
    }

    public void setProfilPersonne(ProfilPersonne profilPersonne) {
        this.profilPersonne = profilPersonne;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }
}
