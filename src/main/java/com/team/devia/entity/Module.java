package com.team.devia.entity;

import javax.persistence.*;

@Entity
@Table(name = "module")
public class Module {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "reference")
    private String reference;

    @Column(name = "lien_image")
    private String lienImage;

    @Column(name = "prixHT")
    private Float prixHT;

    @Column(name = "temps_pose")
    private String tempsPose;

    @Column(name = "pourcentage_tva")
    private String pourcentageTVA;

    @Column(name = "dimension")
    private Float dimension;

    @Column(name = "lien_vers_apercu")
    private String lienVersApercu;

    @Column(name = "lien_vers_plan")
    private String lienVersPlan;

    @Column(name = "nom_fichier_plan")
    private String nomFichierPlan;

    @JoinColumn(name = "id_personne")
    @ManyToOne
    private Personne personne;

    @JoinColumn(name = "id_sous_categorie_module")
    @ManyToOne
    private SousCategorieModule sousCategorieModule;

    /*@OneToMany(mappedBy="maison_modulaire")
    private List<MaisonModulaire> maisonModulaires;*/

    /*@OneToMany(mappedBy="composant")
    private List<Composant> composants;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLienImage() {
        return lienImage;
    }

    public void setLienImage(String lienImage) {
        this.lienImage = lienImage;
    }

    public Float getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(Float prixHT) {
        this.prixHT = prixHT;
    }

    public String getTempsPose() {
        return tempsPose;
    }

    public void setTempsPose(String tempsPose) {
        this.tempsPose = tempsPose;
    }

    public String getPourcentageTVA() {
        return pourcentageTVA;
    }

    public void setPourcentageTVA(String pourcentageTVA) {
        this.pourcentageTVA = pourcentageTVA;
    }

    public Float getDimension() {
        return dimension;
    }

    public void setDimension(Float dimension) {
        this.dimension = dimension;
    }

    public String getLienVersApercu() {
        return lienVersApercu;
    }

    public void setLienVersApercu(String lienVersApercu) {
        this.lienVersApercu = lienVersApercu;
    }

    public String getLienVersPlan() {
        return lienVersPlan;
    }

    public void setLienVersPlan(String lienVersPlan) {
        this.lienVersPlan = lienVersPlan;
    }

    public String getNomFichierPlan() {
        return nomFichierPlan;
    }

    public void setNomFichierPlan(String nomFichierPlan) {
        this.nomFichierPlan = nomFichierPlan;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public SousCategorieModule getSousCategorieModule() {
        return sousCategorieModule;
    }

    public void setSousCategorieModule(SousCategorieModule sousCategorieModule) {
        this.sousCategorieModule = sousCategorieModule;
    }

    /*public List<MaisonModulaire> getMaisonModulaires() {
        return maisonModulaires;
    }

    public void setMaisonModulaires(List<MaisonModulaire> maisonModulaires) {
        this.maisonModulaires = maisonModulaires;
    }

    public List<Composant> getComposants() {
        return composants;
    }

    public void setComposants(List<Composant> composants) {
        this.composants = composants;
    }*/
}
