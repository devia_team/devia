package com.team.devia.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "echeance")
public class Echeance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "montant")
    private Float montant;

    @Column(name = "date_echeance")
    private Date dateEcheance;

    @JoinColumn(name = "id_devis")
    @ManyToOne
    private Devis devis;

    @JoinColumn(name = "id_modele_echeance")
    @ManyToOne
    private ModeleEcheance modeleEcheance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Devis getDevis() {
        return devis;
    }

    public void setDevis(Devis devis) {
        this.devis = devis;
    }

    public ModeleEcheance getModeleEcheance() {
        return modeleEcheance;
    }

    public void setModeleEcheance(ModeleEcheance modeleEcheance) {
        this.modeleEcheance = modeleEcheance;
    }
}
