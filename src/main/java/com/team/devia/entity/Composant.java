package com.team.devia.entity;

import javax.persistence.*;

@Entity
@Table(name = "composant")
public class Composant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "reference")
    private String reference;

    /*@OneToMany(mappedBy="module")
    private List<Module> modules;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
