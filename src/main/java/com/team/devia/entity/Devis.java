package com.team.devia.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Date;

@Entity
@Table(name = "devis")
public class Devis {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "date_emission")
    private Date dateEmission;

    @Column(name = "reference")
    private String reference;

    @Column(name = "date_validation")
    private Date dateValidation;

    @Column(name = "commentaire")
    private String commentaire;

    @Column(name = "taux_tva")
    private Float tauxTVA;

    @JoinColumn(name = "id_personne")
    @ManyToOne
    private Personne personne;

    @JoinColumn(name = "id_moyen_paiement")
    @ManyToOne
    private MoyenPaiement moyenPaiement;

    @JoinColumn(name = "id_etat_devis")
    @ManyToOne
    private EtatDevis etatDevis;

    @ManyToMany
    @JoinTable(
            name="module_devis",
            joinColumns=@JoinColumn(name="id_devis", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="id", referencedColumnName="id"))
    private List<Module> modules;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(Date dateEmission) {
        this.dateEmission = dateEmission;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Float getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(Float tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public MoyenPaiement getMoyenPaiement() {
        return moyenPaiement;
    }

    public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
        this.moyenPaiement = moyenPaiement;
    }

    public EtatDevis getEtatDevis() {
        return etatDevis;
    }

    public void setEtatDevis(EtatDevis etatDevis) {
        this.etatDevis = etatDevis;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }
}
