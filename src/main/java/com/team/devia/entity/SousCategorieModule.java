package com.team.devia.entity;

import javax.persistence.*;

@Entity
@Table(name = "sous_categorie_module")
public class SousCategorieModule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @JoinColumn(name = "id_categorie_module")
    @ManyToOne
    private CategorieModule categorieModule;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public CategorieModule getCategorieModule() {
        return categorieModule;
    }

    public void setCategorieModule(CategorieModule categorieModule) {
        this.categorieModule = categorieModule;
    }
}
