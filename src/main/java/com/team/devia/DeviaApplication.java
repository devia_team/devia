package com.team.devia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeviaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeviaApplication.class, args);
	}
}
