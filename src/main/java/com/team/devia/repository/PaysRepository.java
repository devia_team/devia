package com.team.devia.repository;

import com.team.devia.entity.Pays;
import org.springframework.data.repository.CrudRepository;


public interface PaysRepository extends CrudRepository<Pays, Long> {
}
