package com.team.devia.repository;

import com.team.devia.entity.Module;
import com.team.devia.entity.MoyenPaiement;
import org.springframework.data.repository.CrudRepository;

public interface MoyenPaiementRepository extends CrudRepository<MoyenPaiement, Long> {
}
