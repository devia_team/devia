package com.team.devia.repository;

import com.team.devia.entity.CategorieModule;
import org.springframework.data.repository.CrudRepository;

public interface CategorieModuleRepository extends CrudRepository<CategorieModule, Long> {
}
