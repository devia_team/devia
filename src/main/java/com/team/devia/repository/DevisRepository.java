package com.team.devia.repository;

import com.team.devia.entity.Devis;
import org.springframework.data.repository.CrudRepository;

public interface DevisRepository extends CrudRepository<Devis, Long> {

}
