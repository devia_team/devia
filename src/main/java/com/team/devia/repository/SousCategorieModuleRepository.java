package com.team.devia.repository;

import com.team.devia.entity.SousCategorieModule;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SousCategorieModuleRepository extends CrudRepository<SousCategorieModule, Long> {
    List<SousCategorieModule> findAllByCategorieModule_Id(Long categorieModuleId);
}
