package com.team.devia.repository;


import com.team.devia.entity.EtatDevis;
import org.springframework.data.repository.CrudRepository;

public interface EtatDevisRepository extends CrudRepository<EtatDevis, Long> {
}
