package com.team.devia.repository;


import com.team.devia.entity.Adresse;
import com.team.devia.entity.Commune;
import org.springframework.data.repository.CrudRepository;

public interface AdresseRepository extends CrudRepository<Adresse, Long> {
}
