package com.team.devia.repository;

import com.team.devia.entity.Personne;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface PersonneRepository extends CrudRepository<Personne, Long> {
    Personne findByAdresseMail(String adresseMail);
    List<Personne> findAllByProfilPersonne_Id(Long profilPersonne);
}
