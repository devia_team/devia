package com.team.devia.repository;

import com.team.devia.entity.Commune;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommuneRepository extends CrudRepository<Commune, Long> {
    List<Commune> findCommuneByDepartement_Id(Long id);
}