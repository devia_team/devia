package com.team.devia.repository;


import com.team.devia.entity.Personne;
import com.team.devia.entity.ProfilPersonne;
import org.springframework.data.repository.CrudRepository;

public interface ProfilPersonneRepository extends CrudRepository<ProfilPersonne, Long> {
}
