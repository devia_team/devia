package com.team.devia.repository;

import com.team.devia.entity.Departement;
import org.springframework.data.repository.CrudRepository;


public interface DepartementRepository extends CrudRepository<Departement, Long> {

}
