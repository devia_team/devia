package com.team.devia.repository;

import com.team.devia.entity.Module;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ModuleRepository extends CrudRepository<Module, Long> {
    List<Module> findAllBySousCategorieModule_Id(Long souscategorieid);
}
